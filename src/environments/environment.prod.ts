export const environment: {
  production: boolean;
  restPathRoot: string;
  restServiceRoot: string;
  security: 'csrf' | 'jwt';
} = {
  production: true,
  restPathRoot: '', 
  restServiceRoot: 'services/rest/',
  security: 'csrf',
};
