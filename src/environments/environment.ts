export const environment: {
  production: boolean;
  restPathRoot: string;
  restServiceRoot: string;
  security: 'csrf' | 'jwt';
} = {
  production: true,
  restPathRoot: 'api/', 
  restServiceRoot: 'api/services/rest/',
  security: 'csrf',
};
