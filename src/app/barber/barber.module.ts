import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { ScheduleApiService } from '@core/api-services/schedule-api.service';
import { UsersApiService } from "@app/core/api-services/users-api.service";
import { ServicesApiService } from '@core/api-services/services-api.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitialPageComponent } from './initial-page/initial-page.component';

import { CoreModule } from '../core/core.module';
import { LayoutModule } from '../layout/layout.module';
import { BarberComponent } from './barber.component';
import { BarberRoutingModule } from './barber-routing.module';
import { ServicesComponent } from './services/services.component';
import { ServiceDialogComponent } from './services/dialog/service-dialog.component';
import { ServiceAssignmentComponent } from './service-assignment/service-assignment.component';
import { AssignServiceDialogComponent } from './service-assignment/assign-service-dialog/assign-service-dialog.component';
import { NgbTimepickerModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ScheduleComponent } from './service-assignment/schedule/schedule.component';
import { BookingDayComponent } from './booking-day/booking-day.component';
import { BookingDayAllComponent } from './booking-day/booking-day-all/booking-day-all.component';
import { CancelBookingDlgComponent } from './booking-day/dialogs/cancel-booking-dlg/cancel-booking-dlg.component';
import { UserDetailsDlgComponent } from './booking-day/dialogs/user-details-dlg/user-details-dlg.component';
import { BookingDayOptionsDlgComponent } from '@app/barber/booking-day/dialogs/booking-day-options-dlg/booking-day-options-dlg.component';
import { BookingDayEmployeeComponent } from './booking-day/booking-day-employee/booking-day-employee.component';
import { BookingDayServiceComponent } from './booking-day/booking-day-service/booking-day-service.component';
import { BookingDayCanceledComponent } from './booking-day/booking-day-canceled/booking-day-canceled.component';
import { ClientsComponent } from './clients/clients.component';
import { SearchClientComponent } from './clients/search-client/search-client.component';
import { AddClientComponent } from './clients/add-client/add-client.component';
import { ClientDetailsDlgComponent } from './clients/client-details-dlg/client-details-dlg.component';
import { MyCalendarComponent } from './my-calendar/my-calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    LayoutModule,
    BarberRoutingModule, 
    NgbTimepickerModule,
    NgbModalModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })  
  ],
  declarations: [
    BarberComponent,
  InitialPageComponent,
  ServicesComponent,
  ServiceDialogComponent,
  ServiceAssignmentComponent,
  AssignServiceDialogComponent,
  ScheduleComponent,
  BookingDayComponent,
  BookingDayAllComponent,
  BookingDayOptionsDlgComponent,
  CancelBookingDlgComponent,
  UserDetailsDlgComponent,
  BookingDayEmployeeComponent,
  BookingDayServiceComponent,
  BookingDayCanceledComponent,
  ClientsComponent,
  SearchClientComponent,
  AddClientComponent,
  ClientDetailsDlgComponent,
  MyCalendarComponent
  ],
  exports: [BarberComponent],
  providers:[
    ServicesApiService, 
    UsersApiService, 
    ScheduleApiService,
    BookingApiService
  ],
  entryComponents: [
    ServiceDialogComponent,
    AssignServiceDialogComponent,
    BookingDayOptionsDlgComponent,
    CancelBookingDlgComponent,
    UserDetailsDlgComponent,
    ClientDetailsDlgComponent
  ],
})
export class BarberModule { }
