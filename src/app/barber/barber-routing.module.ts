import { MyCalendarComponent } from './my-calendar/my-calendar.component';
import { BookingDayComponent } from './booking-day/booking-day.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { BarberComponent } from './barber.component';
import { Role } from '@core/security/roles';
import { AuthGuard } from '@core/security/auth-guard.service';
import { ServicesComponent } from '@app/barber/services/services.component';
import { ServiceAssignmentComponent } from '@app/barber/service-assignment/service-assignment.component';
import { ClientsComponent } from '@app/barber/clients/clients.component';

const routes: Routes = [
    {       
        path: '', 
        component: BarberComponent,
        canActivate: [AuthGuard],
        data:{role:Role.EMPLOYEE},
        children: [
          {
            path: '',
            redirectTo: '/barber/initialPage',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },
          {
            path: 'initialPage',
            component: InitialPageComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },
          {
            path: 'services',
            component: ServicesComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },
          {
            path: 'service-assignment',
            component: ServiceAssignmentComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },   
          {
            path: 'booking-day',
            component: BookingDayComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },
          {
            path: 'clients',
            component: ClientsComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },
          {
            path: 'mycalendar',
            component: MyCalendarComponent,
            canActivate: [AuthGuard],
            data:{role:Role.EMPLOYEE}, 
          },          
        ], 
      },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarberRoutingModule { }
