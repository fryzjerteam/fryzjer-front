import { NaviService } from '@core/services/navi.service';
import { AppInterface } from '@core/interfaces/app-interface';
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { TdMediaService } from '@covalent/core';

@Component({
  selector: 'app-barber',
  templateUrl: '../layout/main-content.component.html',
  styleUrls: ['./barber.component.scss'],
  providers: [NaviService]
})
export class BarberComponent implements OnInit {

  constructor(
    private router: Router,
    public media: TdMediaService,
    private naviService: NaviService
  ) { }
  private naviItems: AppInterface.NaviItem[];
  private cardTitle: string;
  private data: any[] = ['sample'];

  sideNavOpened: boolean = false;

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onToggle(value: boolean): void {
    this.sideNavOpened = value;
  }

  close(title: string): void {
    this.sideNavOpened = false;
    if(title == "home"){
      title = "barber.home";
    }
    this.cardTitle = title;
  }

  ngOnInit() {
    this.naviItems = this.naviService.getBarberNaviItems();    
    this.cardTitle = "barber.home";    
  }
  onActivate() {
    if(this.naviService.getTitle() != null){
      this.cardTitle = this.naviService.getTitle();
      this.naviService.setTitle(null);
    }
  }
}
