import { ApplicationSettingsService } from '@core/shared/application-settings.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CalendarEventTimesChangedEvent, CalendarEvent, CalendarEventAction } from 'angular-calendar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GenBarber } from '@app/core/dtos/barber-dto';
import * as moment from 'moment'
import { Subject } from 'rxjs';

@Component({
  selector: 'app-my-calendar',
  templateUrl: './my-calendar.component.html',
  styleUrls: ['./my-calendar.component.scss']
})
export class MyCalendarComponent implements OnInit {

  constructor(
    private modal: NgbModal,
    private bookingApiService: BookingApiService,
    private exceptionsService: ExceptionsService,
    private appSettingsService: ApplicationSettingsService
  ) { }

  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  viewDate: Date = new Date();
  private searchRequest: GenBarber.SearchBookingsDts = {
    userDto: null,
    serviceDto: null,
    startDate: null,
    endDate: null
  };
  private bookings: GenBarber.BookingDto[];
  private appDuration: number;
  private refresh: Subject<any> = new Subject();
  private dayStartHour: number = 6;
  private dayEndHour: number = 23;
  private presentWeekStart: Date;
  private tempWeekStart: Date;
  private nextBtnEnable: boolean = true;
  private prevBtnEnable: boolean = false;
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  ngOnInit() {
    this.createSearchRequest();
    this.appSettingsService.getUnitOffsetTime().subscribe(
      res => {
        this.appDuration = +res.body;
        this.getBookings();
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    );
  }

  private getBookings() {
    this.bookingApiService.getBookingsByCurrentEmployee(this.searchRequest).subscribe(
      res => {
        this.bookings = res.body;
        this.mapBookingsToEvents(this.bookings);
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  private createSearchRequest() {
    let now = new Date(Date.now());
    this.presentWeekStart = moment(now).subtract(now.getDay(), 'd').toDate();
    this.tempWeekStart = this.presentWeekStart;
    this.setDateInSearchRequest(this.tempWeekStart);
  }

  private setDateInSearchRequest(startDate: Date){
    let endDate = moment(startDate).add(6, 'd').toDate();
    this.searchRequest.startDate = {
      month: startDate.getMonth() + 1,
      year: startDate.getFullYear(),
      day: startDate.getDate()
    };
    this.searchRequest.endDate = {
      month: endDate.getMonth() + 1,
      year: endDate.getFullYear(),
      day: endDate.getDate()
    }
  }

  events: CalendarEvent[] = [

  ];

  mapBookingsToEvents(bookings: GenBarber.BookingDto[]) {
    let first: boolean = true;
    this.events.length = 0;
    for (let booking of bookings) {
      let startDateDto = booking.startDateTimeDto.localDateDto;
      let startTimeDto = booking.startDateTimeDto.localTimeDto;
      let startDate = new Date(startDateDto.year, startDateDto.month - 1, startDateDto.day, startTimeDto.hour, startTimeDto.minute);

      let serviceDuration = booking.serviceAssignmentDto.serviceDto.durationUnits * this.appDuration;
      let endDate = moment(startDate).add(serviceDuration, 'm').toDate();

      let event = {
        start: startDate,
        end: endDate,
        title: booking.client.firstName + " " + booking.client.lastName + " - "+ booking.serviceAssignmentDto.serviceDto.name,
        actions: this.actions
      }
      this.events.push(event);
      this.setDayHourTime(startTimeDto.hour, endDate.getHours(), first);
      first = false;
    }
    this.refresh.next();
  }

  private setDayHourTime(startTime: number, endTime: number, first: boolean){
    if(first == true){
      this.dayStartHour = startTime;
      this.dayEndHour = endTime + 1;
    }
    else{
      if(this.dayStartHour > startTime){
        this.dayStartHour = startTime;
      }
      if(this.dayEndHour <= endTime ){
        this.dayEndHour = endTime + 1;
      }
    }
  }

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    //this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  onPrevClick(){
    this.viewDate = moment(this.viewDate).subtract(7,'d').toDate();
    this.tempWeekStart = moment(this.tempWeekStart).subtract(7,'d').toDate();
    this.setDateInSearchRequest(this.tempWeekStart);
    this.getBookings();
    this.nextBtnEnable = true;

    if(moment(this.presentWeekStart).diff(this.tempWeekStart,'d') == 0){
      this.prevBtnEnable = false;
    }
  }
  onNextClick(){    
    this.viewDate = moment(this.viewDate).add(7,'d').toDate();
    this.tempWeekStart = moment(this.tempWeekStart).add(7,'d').toDate();
    this.setDateInSearchRequest(this.tempWeekStart);
    this.getBookings();
    this.prevBtnEnable = true;

    if(moment(this.presentWeekStart).diff(this.tempWeekStart, 'd') > 365 ){
      this.nextBtnEnable = false;
    }
  }


}
