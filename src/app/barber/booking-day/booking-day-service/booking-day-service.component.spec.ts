import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDayServiceComponent } from './booking-day-service.component';

describe('BookingDayServiceComponent', () => {
  let component: BookingDayServiceComponent;
  let fixture: ComponentFixture<BookingDayServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDayServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDayServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
