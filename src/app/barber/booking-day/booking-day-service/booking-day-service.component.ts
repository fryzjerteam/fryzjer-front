import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { ServicesApiService } from '@app/core/api-services/services-api.service';

@Component({
  selector: 'app-booking-day-service',
  templateUrl: './booking-day-service.component.html',
  styleUrls: ['../booking-day.component.scss']
})
export class BookingDayServiceComponent {

  @Input() selectedDate: GenBarber.LocalDateDto;
  @Output() bookingSelectedOutput = new EventEmitter<GenBarber.BookingDto>();
  private bookings: GenBarber.BookingDto[] = [];
  private services: GenBarber.ServiceDto[];
  private selectedService: GenBarber.ServiceDto;
  private searchRequest: GenBarber.SearchBookingsDts = {
    userDto: null,
    serviceDto: null,
    startDate: null,
    endDate: null
  };

  constructor(
    private exceptionsService: ExceptionsService,
    private bookingApiService: BookingApiService,
    private servicesApiService: ServicesApiService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.selectedDate != null){
      this.searchRequest.startDate = this.selectedDate;
      this.searchRequest.endDate = this.selectedDate; 
      this.servicesApiService.getAllServices().subscribe(
        res => {
          this.services = res.body;
        },
        err => {
          this.exceptionsService.handleRestException(err);
        }
      )       
    }
  }

  onServiceClick(service: GenBarber.ServiceDto){
    this.selectedService = service;
    this.searchRequest.serviceDto = service;
    this.bookingApiService.getBookingsByService(this.searchRequest).subscribe(
      res => {
        this.bookings = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  private onBookingClick(booking: GenBarber.BookingDto){
    this.bookingSelectedOutput.emit(booking);
  }

}
