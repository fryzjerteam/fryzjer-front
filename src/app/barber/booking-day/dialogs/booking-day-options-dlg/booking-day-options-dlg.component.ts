import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-booking-day-options-dlg',
  templateUrl: './booking-day-options-dlg.component.html'
})
export class BookingDayOptionsDlgComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<BookingDayOptionsDlgComponent>,
    @Inject(MAT_DIALOG_DATA) public options: string[]
  ) { }

  ngOnInit() {
  }

  onOptionClick(option: string): void {
    this.dialogRef.close(option);
  };

}
