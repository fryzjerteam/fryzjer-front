import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDayOptionsDlgComponent } from './booking-day-options-dlg.component';

describe('BookingDayOptionsDlgComponent', () => {
  let component: BookingDayOptionsDlgComponent;
  let fixture: ComponentFixture<BookingDayOptionsDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDayOptionsDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDayOptionsDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
