import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { GenUsers } from '@core/dtos/users-dto';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-details-dlg',
  templateUrl: './user-details-dlg.component.html' 
})
export class UserDetailsDlgComponent implements OnInit {

  private details: GenUsers.UserDetailsDto;

  constructor(
    public dialogRef: MatDialogRef<UserDetailsDlgComponent>,
    @Inject(MAT_DIALOG_DATA) public client: GenUsers.UserDto,
    private usersApiService: UsersApiService,
    public snackBar: MatSnackBar,
    private exceptionsService: ExceptionsService
  ) { }

  ngOnInit() {
    if(this.client != null && this.client.id != null){
      this.usersApiService.getUserDetails(this.client.id).subscribe(
        res => {
          this.details = res.body;
        },
        err => {
          this.exceptionsService.handleRestException(err);
        }
      )
    }
    else{
      this.snackBar.open("Error: No client");
    }
  }

}
