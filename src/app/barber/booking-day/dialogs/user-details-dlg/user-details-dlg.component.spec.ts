import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsDlgComponent } from './user-details-dlg.component';

describe('UserDetailsDlgComponent', () => {
  let component: UserDetailsDlgComponent;
  let fixture: ComponentFixture<UserDetailsDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailsDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
