import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelBookingDlgComponent } from './cancel-booking-dlg.component';

describe('CancelBookingDlgComponent', () => {
  let component: CancelBookingDlgComponent;
  let fixture: ComponentFixture<CancelBookingDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelBookingDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelBookingDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
