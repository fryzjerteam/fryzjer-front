import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cancel-booking-dlg',
  templateUrl: './cancel-booking-dlg.component.html'
})
export class CancelBookingDlgComponent {

  private note: string;

  constructor(
    public dialogRef: MatDialogRef<CancelBookingDlgComponent>,
    public snackBar: MatSnackBar
  ) { }

  private onYesClick(){
    if(this.note != null && this.note.length > 10){
      this.dialogRef.close(this.note);
    }
    else{
      this.snackBar.open("Minimum 10 characters");
    }
  }
  
}
