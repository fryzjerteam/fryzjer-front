import { UsersApiService } from './../../../core/api-services/users-api.service';
import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { GenUsers } from '@app/core/dtos/users-dto';
import { Role } from '@app/core/security/roles';

@Component({
  selector: 'app-booking-day-employee',
  templateUrl: './booking-day-employee.component.html',
  styleUrls: ['../booking-day.component.scss']
})
export class BookingDayEmployeeComponent {

  @Input() selectedDate: GenBarber.LocalDateDto;
  @Output() bookingSelectedOutput = new EventEmitter<GenBarber.BookingDto>();
  private bookings: GenBarber.BookingDto[] = [];
  private employees: GenUsers.UserDto[];
  private selectedEmployee: GenUsers.UserDto;
  private searchRequest: GenBarber.SearchBookingsDts = {
    userDto: null,
    serviceDto: null,
    startDate: null,
    endDate: null
  };

  constructor(
    private exceptionsService: ExceptionsService,
    private bookingApiService: BookingApiService,
    private usersApiService: UsersApiService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.selectedDate != null){
      this.searchRequest.startDate = this.selectedDate;
      this.searchRequest.endDate = this.selectedDate; 
      this.usersApiService.getUsersByRole(Role.EMPLOYEE).subscribe(
        res => {
          this.employees = res.body;
        },
        err => {
          this.exceptionsService.handleRestException(err);
        }
      )       
    }
  }

  onEmployeeClick(employee: GenUsers.UserDto){
    this.selectedEmployee = employee;
    this.searchRequest.userDto = employee;
    this.bookingApiService.getBookingsByEmployee(this.searchRequest).subscribe(
      res => {
        this.bookings = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  private onBookingClick(booking: GenBarber.BookingDto){
    this.bookingSelectedOutput.emit(booking);
  }

}
