import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDayEmployeeComponent } from './booking-day-employee.component';

describe('BookingDayEmployeeComponent', () => {
  let component: BookingDayEmployeeComponent;
  let fixture: ComponentFixture<BookingDayEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDayEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDayEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
