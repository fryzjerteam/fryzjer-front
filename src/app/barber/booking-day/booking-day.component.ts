import { ExceptionsService } from '@exceptions/exceptions.service';
import { BookingApiService } from '@core/api-services/booking-api.service';
import { GenBarber } from '@core/dtos/barber-dto';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { BookingDayOptionsDlgComponent } from '@app/barber/booking-day/dialogs/booking-day-options-dlg/booking-day-options-dlg.component';
import { CancelBookingDlgComponent } from '@app/barber/booking-day/dialogs/cancel-booking-dlg/cancel-booking-dlg.component';
import { UserDetailsDlgComponent } from '@app/barber/booking-day/dialogs/user-details-dlg/user-details-dlg.component';


@Component({
  selector: 'app-booking-day',
  templateUrl: './booking-day.component.html',
  styleUrls: ['./booking-day.component.scss']
})
export class BookingDayComponent implements OnInit {

  private readonly BOOKING_OPT_CANCEL_BOOKING = "bookingDay.cancelBooking";
  private readonly BOOKING_OPT_CLIENT_DETAILS = "bookingDay.clientDetails";
  private readonly VIEW_OPTION_ALL = "bookingDay.all";
  private readonly VIEW_OPTION_EMPLOYEE = "bookingDay.employee";
  private readonly VIEW_OPTION_SERVICE = "bookingDay.service";
  private readonly VIEW_OPTION_CANCELED = "bookingDay.canceled";

  private datePicker: Date;
  private viewOptions: string[] = [
    this.VIEW_OPTION_ALL,
    this.VIEW_OPTION_EMPLOYEE,
    this.VIEW_OPTION_SERVICE,
    this.VIEW_OPTION_CANCELED
  ];
  private selectedOption: string;
  private bookingOptions: string[] = [
    this.BOOKING_OPT_CANCEL_BOOKING,
    this.BOOKING_OPT_CLIENT_DETAILS
  ];
  private selectedDate: GenBarber.LocalDateDto;

  constructor(
    public dialog: MatDialog,
    private bookingApiService: BookingApiService,
    private exceptionsService: ExceptionsService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.selectedOption = this.viewOptions[0];
  }

  private onDateChange(event) {
    this.selectedDate = this.getPickerDate(); 
  }

  private getPickerDate(): GenBarber.LocalDateDto {
    if (this.datePicker != null) {
      return {
        month: this.datePicker.getMonth() + 1,
        year: this.datePicker.getFullYear(),
        day: this.datePicker.getDate()
      }
    }
  }

  private onBookingSelected(booking: GenBarber.BookingDto) {
    this.openBookingOptionsDlg(booking);
  }

  private openBookingOptionsDlg(booking: GenBarber.BookingDto): void {
    const dialogRef = this.dialog.open(BookingDayOptionsDlgComponent, {
      data: this.bookingOptions
    });

    dialogRef.afterClosed().subscribe(option => {
      if (option == this.BOOKING_OPT_CANCEL_BOOKING) {
        this.cancelBooking(booking);
      }
      if (option == this.BOOKING_OPT_CLIENT_DETAILS) {
        this.showUserDetails(booking);
      }
    });
  }

  private cancelBooking(booking: GenBarber.BookingDto) {
    const dialogRef = this.dialog.open(CancelBookingDlgComponent);

    dialogRef.afterClosed().subscribe(res => {
      if (res != null && res != "") {
        let cancel: GenBarber.CancelBookingDto = {
          bookingId: booking.id,
          note: res
        }
        this.bookingApiService.cancelBooking(cancel).subscribe(
          res => {
            this.snackBar.open('Booking canceled');
            this.refreshBookings();
          },
          err => {
            this.exceptionsService.handleRestException(err);
          }
        );
      }
    });
  }

  private refreshBookings(){
    this.selectedDate = this.getPickerDate(); 
  }

  private showUserDetails(booking: GenBarber.BookingDto) {

    const dialogRef = this.dialog.open(UserDetailsDlgComponent, {
      width: '30%',
      data: booking.client
    });

    dialogRef.afterClosed().subscribe(res => {
    });
  }


}
