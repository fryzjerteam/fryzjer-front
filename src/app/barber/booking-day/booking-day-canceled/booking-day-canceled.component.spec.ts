import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDayCanceledComponent } from './booking-day-canceled.component';

describe('BookingDayCanceledComponent', () => {
  let component: BookingDayCanceledComponent;
  let fixture: ComponentFixture<BookingDayCanceledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDayCanceledComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDayCanceledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
