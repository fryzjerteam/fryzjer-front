import { Component, OnInit, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { BookingApiService } from '@app/core/api-services/booking-api.service';

@Component({
  selector: 'app-booking-day-canceled',
  templateUrl: './booking-day-canceled.component.html',
  styleUrls: ['../booking-day.component.scss']
})
export class BookingDayCanceledComponent{

  @Input() selectedDate: GenBarber.LocalDateDto;
  @Output() bookingSelectedOutput = new EventEmitter<GenBarber.BookingDto>();
  private bookings: GenBarber.BookingDto[];

  constructor(
    private exceptionsService: ExceptionsService,
    private bookingApiService: BookingApiService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.selectedDate != null) {
      let searchRequest: GenBarber.SearchBookingsDts = {
        userDto: null,
        serviceDto: null,
        startDate: this.selectedDate,
        endDate: this.selectedDate
      } 
      console.log("bimbmim");
      this.bookingApiService.getBookingsCanceledByDay(searchRequest).subscribe(
        res => {
          this.bookings = res.body;
          console.log("ram");
        },
        err => {
          this.exceptionsService.handleRestException(err);
        }
      )
    }
  }

  private onBookingClick(booking: GenBarber.BookingDto) {
    //this.bookingSelectedOutput.emit(booking);
  }

}
