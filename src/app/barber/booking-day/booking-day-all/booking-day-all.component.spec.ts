import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDayAllComponent } from './booking-day-all.component';

describe('BookingDayAllComponent', () => {
  let component: BookingDayAllComponent;
  let fixture: ComponentFixture<BookingDayAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDayAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDayAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
