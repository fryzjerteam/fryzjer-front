import { BookingApiService } from '@core/api-services/booking-api.service';
import { ExceptionsService } from '@exceptions/exceptions.service';
import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';

@Component({
  selector: 'app-booking-day-all',
  templateUrl: './booking-day-all.component.html',
  styleUrls: ['../booking-day.component.scss']
})
export class BookingDayAllComponent{

  @Input() selectedDate: GenBarber.LocalDateDto;
  @Output() bookingSelectedOutput = new EventEmitter<GenBarber.BookingDto>();
  private bookings: GenBarber.BookingDto[];

  constructor(  
    private exceptionsService: ExceptionsService,
    private bookingApiService: BookingApiService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.selectedDate != null){
      let searchRequest: GenBarber.SearchBookingsDts =  {
        userDto: null,
        serviceDto: null,
        startDate: this.selectedDate,
        endDate: this.selectedDate
      }
      this.bookingApiService.getBookingsByDay(searchRequest).subscribe(
        res => {
          this.bookings = res.body;
        },
        err => {
          this.exceptionsService.handleRestException(err);
        }
      )
    }
  }

  private onBookingClick(booking: GenBarber.BookingDto){
    this.bookingSelectedOutput.emit(booking);
  }

}
