import { ServicesApiService } from '@core/api-services/services-api.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { GenBarber } from '@core/dtos/barber-dto';
import { ExceptionsService } from '@exceptions/exceptions.service';
import { GenUsers } from '@core/dtos/users-dto';
import { Component, OnInit } from '@angular/core';
import { UsersApiService } from "@app/core/api-services/users-api.service";
import { Role } from '@app/core/security/roles';
import { AssignServiceDialogComponent } from '@app/barber/service-assignment/assign-service-dialog/assign-service-dialog.component';

@Component({
  selector: 'app-service-assignment',
  templateUrl: './service-assignment.component.html',
  styleUrls: ['./service-assignment.component.scss']
})
export class ServiceAssignmentComponent implements OnInit {

  constructor(
    private UsersApiService: UsersApiService,
    private exceptionsService: ExceptionsService,
    private servicesApiService: ServicesApiService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }
  employees: GenUsers.UserDto[];
  services: GenBarber.ServiceDto[];
  currentEmployee: GenUsers.UserDto;

  ngOnInit() {
    this.UsersApiService.getUsersByRole(Role.EMPLOYEE).subscribe(
      (res: any) => {
        this.employees = res.body;
      },
      (err: any) => {
        this.exceptionsService.handleRestException(err);
      })
  }

  expandEmployee(employee: GenUsers.UserDto) {
    this.findServiceAssignments(employee);
    this.currentEmployee = employee;
  }

  findServiceAssignments(employee: GenUsers.UserDto) {
    this.servicesApiService.findServiceAssignmentsByUser(employee).subscribe(res => {
      this.services = res.body;
    },
      err => {
        this.exceptionsService.handleRestException(err);
      })
  }

  onAssignServiceClick(employee: GenUsers.UserDto) {

    this.servicesApiService.getAllServices().subscribe((res: any) => {
      let allServices: GenBarber.ServiceDto[] = res.body;

      const dialogRef = this.dialog.open(AssignServiceDialogComponent, {
        data: allServices
      });

      dialogRef.afterClosed().subscribe(result => {
        let serviceIds = this.services.map(s => s.id);
        if (result != null && !serviceIds.includes(result.id)) {
          this.assignService(result, employee);
        }
        else if(result != null){
          this.snackBar.open("Assignment already exists!");
        }
      });
    },
      (err: any) => {
        this.exceptionsService.handleRestException(err);
      })
  }

  private assignService(serviceToAssign: GenBarber.ServiceDto, employee: GenUsers.UserDto): void {
    let dts = {
      serviceDto: serviceToAssign,
      userDto: employee
    }
    this.servicesApiService.assignService(dts).subscribe(res => {
      this.expandEmployee(employee);
    },
      err => { this.exceptionsService.handleRestException(err); })
  }

  private onAssignmentDelete(employee: GenUsers.UserDto, service: GenBarber.ServiceDto){
    let assignment: GenBarber.ServiceAssignmentDto = {
      serviceDto: service,
      userDto: employee
    }
    this.servicesApiService.deleteServiceAssignment(assignment).subscribe(
      res => {
        this.findServiceAssignments(assignment.userDto);
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

}
