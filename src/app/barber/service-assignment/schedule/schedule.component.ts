import { MatSnackBar } from '@angular/material';
import { GenBarber } from '@core/dtos/barber-dto';
import { ExceptionsService } from './../../../exceptions/exceptions.service';
import { Component, OnInit, Input } from '@angular/core';
import { OnChanges } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { GenUsers } from '@app/core/dtos/users-dto';
import { ScheduleApiService } from '@core/api-services/schedule-api.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit, OnChanges {

  @Input() employee: GenUsers.UserDto;
  selectedEmployee: GenUsers.UserDto = null;
  schedule: GenBarber.ScheduleDto = {
    id: null,
    userDto: null,
    days: null
  };

  constructor(
    private scheduleApiService: ScheduleApiService,
    private exceptionsService: ExceptionsService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let propName in changes) {
      let chng = changes[propName];
      this.selectedEmployee = chng.currentValue;
    }
    console.log("sel employee: ", this.selectedEmployee);
    if (this.selectedEmployee != null) {
      this.scheduleApiService.findScheduleByEmployee(this.selectedEmployee).subscribe(res => {
        this.schedule = res.body[0];
      },
        err => {
          this.exceptionsService.handleRestException(err);
        })
    }
  }

  onUpdateClick() {
    this.scheduleApiService.updateSchedule(this.schedule).subscribe(
      res => {
        this.snackBar.open("Updated");
        this.schedule = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    );
  }

  onResetClick() {
    if (this.schedule != null) {
      for (let day of this.schedule.days) {
        this.resetTime(day.startTime);
        this.resetTime(day.endTime);
      }
    }
  }

  private resetTime(time: GenBarber.LocalTimeDto) {
    time.hour = 0;
    time.minute = 0;
    time.second = 0;
  }

}
