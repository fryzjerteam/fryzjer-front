import { GenBarber } from '@core/dtos/barber-dto';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { ExceptionsService } from '@app/exceptions/exceptions.service';

@Component({
  selector: 'app-assign-service-dialog',
  templateUrl: './assign-service-dialog.component.html',
  styleUrls: ['./assign-service-dialog.component.scss']
})
export class AssignServiceDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AssignServiceDialogComponent>,
    private exceptionsService: ExceptionsService,
    @Inject(MAT_DIALOG_DATA) public data: GenBarber.ServiceDto[]) {}
  

  ngOnInit() {
  }

  onServiceClick(service: GenBarber.ServiceDto){
    this.dialogRef.close(service);
  }

}
