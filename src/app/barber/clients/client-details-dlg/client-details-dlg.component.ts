import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { GenUsers } from '@app/core/dtos/users-dto';

@Component({
  selector: 'app-client-details-dlg',
  templateUrl: './client-details-dlg.component.html'
})
export class ClientDetailsDlgComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ClientDetailsDlgComponent>,
    @Inject(MAT_DIALOG_DATA) public userId: number,
    private usersApiService: UsersApiService,
    private exceptionsService: ExceptionsService
  ) { }

  private userData: GenUsers.UserDataDts = {
    userDto: {
      id: null,
      firstName: "",
      lastName: "",
      name: "",
      role: ""
    },
    userDetailsDto: {
      address: "",
      email: "",
      phone: "",
      userId: null
    }
  }

  ngOnInit() {
    this.usersApiService.getUser(this.userId).subscribe(
      (res: any ) => {
        this.userData = res.body;
      },
      (err: any ) => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

}
