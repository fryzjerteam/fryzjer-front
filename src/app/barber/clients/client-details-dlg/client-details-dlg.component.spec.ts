import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDetailsDlgComponent } from './client-details-dlg.component';

describe('ClientDetailsDlgComponent', () => {
  let component: ClientDetailsDlgComponent;
  let fixture: ComponentFixture<ClientDetailsDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDetailsDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDetailsDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
