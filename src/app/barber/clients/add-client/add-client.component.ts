import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { GenUsers } from '@app/core/dtos/users-dto';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent {

  constructor(
    private usersApiService: UsersApiService,
    private exceptionService: ExceptionsService,
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) { }

  private addUserForm = this.formBuilder.group({
    firstName: [""],
    lastName: [""],
    address: [""],
    email: [""],
    phone: [""]
  })

  onAddButton() {
    let userDataDts: GenUsers.UserRegisterDataDts = {

      basicUserDto: {
        id: null,
        name: null,
        password: null,
        firstName: this.addUserForm.get("firstName").value,
        lastName: this.addUserForm.get("lastName").value,
        role: null
      },
      userDetailsDto: {
        userId: null,
        address: this.addUserForm.get("address").value,
        email: this.addUserForm.get("email").value,
        phone: this.addUserForm.get("phone").value,
      }
    };

    this.usersApiService.registerClient(userDataDts).subscribe( 
      (res: any) => {
        let receivedUserData: GenUsers.UserDataDts = res.body;
        if (receivedUserData == null) {
          this.snackBar.open("User with this login already exists");
        }
        else {
          this.snackBar.open('OK');
        }
      },
      (err: any) => {
        this.exceptionService.handleRestException(err);
      },
    ) 
  }

}
