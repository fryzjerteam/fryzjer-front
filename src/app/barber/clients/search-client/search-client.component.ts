import { ClientDetailsDlgComponent } from './../client-details-dlg/client-details-dlg.component';
import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { GenUsers } from '@app/core/dtos/users-dto';

@Component({
  selector: 'app-search-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.scss']
})
export class SearchClientComponent{

  constructor(
    private usersApiService: UsersApiService,
    private exceptionService: ExceptionsService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  private clients: GenUsers.UserDto[];
  private isUserListVisible = false;


  onSearch(name: string) {
    this.usersApiService.searchClients(name).subscribe(
      (res: any) => {
        this.clients = res.body;

        if (this.clients.length == 0) {
          this.snackBar.open("Cannot find the client");
          this.isUserListVisible = false;
        }
        else {
          this.isUserListVisible = true;
        }
      },
      (err: any) => {
        this.exceptionService.handleRestException(err);
      }
    )
  }

  onSelect(user: GenUsers.UserDto) {
    if (user != null && user.id != null) {
      const dialogRef = this.dialog.open(ClientDetailsDlgComponent,
        {
          width: "40%",
          data: user.id
        });
    }
  }

}
