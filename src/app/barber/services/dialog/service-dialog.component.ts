import { ExceptionsService } from '@exceptions/exceptions.service';
import { GenBarber } from '@core/dtos/barber-dto';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { ServicesApiService } from '@core/api-services/services-api.service';

@Component({
  selector: 'app-service-dialog',
  templateUrl: './service-dialog.component.html',
  styleUrls: ['./service-dialog.component.scss']
})
export class ServiceDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ServiceDialogComponent>,
    private servicesApiService: ServicesApiService,
    private exceptionsService: ExceptionsService,
    @Inject(MAT_DIALOG_DATA) public data: {
      service: GenBarber.ServiceDto,
      durationInMinutes: number
    }) {}

   
  onEditClick(){
    this.servicesApiService.updateService(this.data.service).subscribe(
      (res: any) => {
        this.dialogRef.close();
      },
      (err: any) => { this.exceptionsService.handleRestException(err); }
    )
  } 
  
  onDeleteClick(){
    this.servicesApiService.deleteService(this.data.service).subscribe(
      (res: any) => {
        this.dialogRef.close();
      },
      (err: any) => { this.exceptionsService.handleRestException(err); }
    )
  }

  onPlusButton(){
    this.data.service.durationUnits++;
  }

  onMinusButton(){
    this.data.service.durationUnits--;
    if(this.data.service.durationUnits < 1){
      this.data.service.durationUnits = 1;
    }
  }

  ngOnInit() {
  }

}
