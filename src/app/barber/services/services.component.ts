import { ExceptionsService } from '@exceptions/exceptions.service';
import { ServicesApiService } from '@core/api-services/services-api.service';
import { ApplicationSettingsService } from '@core/shared/application-settings.service';
import { Component, OnInit } from '@angular/core';
import { GenBarber } from "@core/dtos/barber-dto";
import { MatDialog } from '@angular/material';
import { ServiceDialogComponent } from '@app/barber/services/dialog/service-dialog.component';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  private newService: GenBarber.ServiceDto = {
    id: null,
    name: null,
    durationUnits: 1
  }
  private durationInMinutes: number = 15;
  private services: GenBarber.ServiceDto[];


  constructor(private appSettingsService: ApplicationSettingsService,
              private servicesApiService: ServicesApiService,
              private exceptionsService: ExceptionsService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.calculateDuration();
    this.getAllServices();
  }

  onAddService(){
    this.servicesApiService.addService(this.newService).subscribe(
      (res: any) => {
        this.services = res.body;
      },
      (err: any) => {this.exceptionsService.handleRestException(err);}
    )
  }

  onSelect(service: GenBarber.ServiceDto){
    const dialogRef = this.dialog.open(ServiceDialogComponent, {
      width: '400px',
      data: {
        service: service,
        durationInMinutes: this.durationInMinutes
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllServices();
    });
  }

  onPlusButton(){
    this.newService.durationUnits++;
  }

  onMinusButton(){
    this.newService.durationUnits--;
    if(this.newService.durationUnits < 1){
      this.newService.durationUnits = 1;
    }
  }

  getAllServices(){
    this.servicesApiService.getAllServices().subscribe(
      (res: any) => {
        this.services = res.body;
      },
      (err: any) => {this.exceptionsService.handleRestException(err);}
    )
  }

  calculateDuration(){
    this.appSettingsService.getUnitOffsetTime().subscribe(
      (res: any) => { 
        this.durationInMinutes = +res.body;
        if(this.durationInMinutes == 0 ){
          this.durationInMinutes = 15;
        }
      },
      (err: any) => { this.durationInMinutes = 15 }
    );
  }

}
