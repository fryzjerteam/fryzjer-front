import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '@core/security/auth.service';
import { LoginService } from '@core/security/login.service';
import { IRole } from '@core/interfaces/roleJson';
import { Role } from '@core/security/roles';

@Component({
  templateUrl: './login.component.html',
})
export class LoginComponent {
  constructor(
    private translate: TranslateService,
    private loginService: LoginService,
    public authService: AuthService,
    private router: Router,
    public snackBar: MatSnackBar,
  ) {}

  login(login: any): void {
    this.loginService
      .login(login.value.username, login.value.password)
      .subscribe(
        (res: any) => {
          let role:IRole = res.body;
          if(Object.values(Role).includes(role.role) ) {
            this.loginService.getCsrf().subscribe((data: any) => {
              this.authService.setToken(data.token);
              this.authService.setLogged(true);
              this.authService.setRole(role.role);
  
              if(role.role == Role.ADMIN){
                this.router.navigate(['/admin']);
              }
              if(role.role == Role.EMPLOYEE){
                this.router.navigate(['/barber']);
              }
              if(role.role == Role.CLIENT){
                this.router.navigate(['/home']);
              }
              
            });
          }
          
        },
        (err: any) => {
          this.authService.setLogged(false);
          this.translate.get('login.errorMsg').subscribe((res: string) => {
            this.snackBar.open(res, 'OK', {
              duration: 5000,
            });
          });
        },
      );
  }
}
