import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { Role } from '../core/security/roles';
import { AuthGuard } from '../core/security/auth-guard.service';

const routes: Routes = [
    {       
        path: '', 
        component: LoginComponent
        //canActivate: [AuthGuard]
      },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
