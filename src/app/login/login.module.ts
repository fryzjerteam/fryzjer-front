import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { CoreModule } from '@core/core.module';
import { LayoutModule } from '../layout/layout.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { LoginRoutingModule } from "./login-routing.module";

@NgModule({
  imports: [
    //CommonModule,
    CoreModule,
    LayoutModule,
    TranslateModule,
    LoginRoutingModule
  ],
  declarations: [ LoginComponent ],
  exports: [ LoginComponent ]
})
export class LoginModule { }
