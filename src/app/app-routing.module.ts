import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/security/auth-guard.service';
//import { LoginComponent } from './login/login.component';
//import { HomeComponent } from './home/home.component';
//import { InitialPageComponent } from './home/initial-page/initial-page.component';
//import { AdminModule } from './admin/admin.module';
import { Role } from './core/security/roles';
import { LoginComponent } from './login/login.component';
import { ExceptionsComponent } from '@app/exceptions/exceptions.component';
//import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  {
    path: 'login',
    //component:LoginComponent,
    //canActivate: [AuthGuard],
    loadChildren:'app/login/login.module#LoginModule' 
  },
  {
    path: 'admin', 
    /*data:{role:Role.Admin}, */
    //component:AdminComponent
    loadChildren: 'app/admin/admin.module#AdminModule'
  },
  {
      path: 'home',
    /* canActivate: [AuthGuard],
    data:{role:Role.CLIENT}, */
    loadChildren:'app/home/home.module#HomeModule'    
  },
  {
    path: 'barber',
    /* canActivate: [AuthGuard],
    data:{role:Role.EMPLOYEE}, */
    loadChildren:'app/barber/barber.module#BarberModule'    
  },
  {
    path: "exception",
    component: ExceptionsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/login',
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
