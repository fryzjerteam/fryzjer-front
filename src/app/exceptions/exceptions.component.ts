import { Component, OnInit } from '@angular/core';
import { ExceptionsService } from "@exceptions/exceptions.service";

@Component({
  selector: 'app-exceptions',
  templateUrl: './exceptions.component.html',
  styleUrls: ['./exceptions.component.scss']
})
export class ExceptionsComponent implements OnInit {
  private errorMsg: string;

  constructor(private exceptionService: ExceptionsService ) { }

  ngOnInit() {
    this.errorMsg = this.exceptionService.getExceptionMsg();
  }

}
