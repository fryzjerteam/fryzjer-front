import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

const UNEXPECTED_ERROR_MSG: string = "Unexpected error occures. We are working on it...";
const UNAUTHORIZED_ERROR_MSG: string = "You are not authorised to view this content."

@Injectable()
export class ExceptionsService {
  private exceptionMsg: string;
  
  constructor(private router: Router) { }
  
  getExceptionMsg() : string {
    return this.exceptionMsg;
  }
  
  handleRestException(err: any){
    if(err != null && err.error != null){
      this.handleBusinessException(err.error.message);
      
    }
    else if(err != null && err.status == 403){
      this.handleUnauthorizedException();
    }
    else{
      this.handleApplicationException();
    }
  }
  
  private handleBusinessException(msg: string){
    this.exceptionMsg = msg;
    this.router.navigate(['exception']);
  }
  private handleApplicationException(){
    this.exceptionMsg = UNEXPECTED_ERROR_MSG;
    this.router.navigate(['exception']);
  }
  private handleUnauthorizedException() {
    this.exceptionMsg = UNAUTHORIZED_ERROR_MSG;
    this.router.navigate(['exception']);
  }
  
}
