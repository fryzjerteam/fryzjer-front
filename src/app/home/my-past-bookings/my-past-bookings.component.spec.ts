import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPastBookingsComponent } from './my-past-bookings.component';

describe('MyPastBookingsComponent', () => {
  let component: MyPastBookingsComponent;
  let fixture: ComponentFixture<MyPastBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPastBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPastBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
