import { Component, OnInit } from '@angular/core';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { GenBarber } from '@app/core/dtos/barber-dto';

@Component({
  selector: 'app-my-past-bookings',
  templateUrl: './my-past-bookings.component.html',
  styleUrls: ['./my-past-bookings.component.scss']
})
export class MyPastBookingsComponent implements OnInit {

  private bookings: GenBarber.BookingDto[];

  constructor(
    private bookingApiService: BookingApiService,
    private exceptionsService: ExceptionsService
  ) { }

  ngOnInit() {
    this.getPastBookings();
  }

  private getPastBookings(){
    this.bookingApiService.getOldClientBookings().subscribe(
      res => {
        this.bookings = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

}
