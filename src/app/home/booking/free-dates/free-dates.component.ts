import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { ExceptionsService } from '@exceptions/exceptions.service';
import { BookingApiService } from '@core/api-services/booking-api.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Inject } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';
import { YesNoDlgComponent } from '@app/core/dialogs/yes-no-dlg/yes-no-dlg.component';

@Component({
  selector: 'app-free-dates',
  templateUrl: './free-dates.component.html',
  styleUrls: ['./free-dates.component.scss']
})
export class FreeDatesComponent implements OnInit, OnChanges{


  @Input() searchRequest: GenBarber.SearchFreeBookingDts;
  freeBookings: GenBarber.FreeBookingDto[];

  constructor(  private bookingApiService: BookingApiService,
                private exceptionsService: ExceptionsService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.searchRequest.currentValue != null){
      this.getFreeBookings();
    } else{
      this.freeBookings = null;
    }
  }

  private getFreeBookings(){
    this.bookingApiService.getFreeBookings(this.searchRequest).subscribe(
      res => {
        this.freeBookings = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  onTimeSelect(freeBooking: GenBarber.FreeBookingDto, startTime: GenBarber.LocalTimeDto){
    const dialogRef = this.dialog.open(YesNoDlgComponent, { 
      data: {
        titel: 'bookings.bookService', 
        content: 'bookings.areYouSure'
      }
     });
    dialogRef.afterClosed().subscribe(result => {
      if(result == true){
        this.bookService(freeBooking, startTime);
      }
    });
  }

  private bookService(freeBooking: GenBarber.FreeBookingDto, startTime: GenBarber.LocalTimeDto){
    let bookingDto: GenBarber.BookingDto = {
      id: null,
      serviceAssignmentDto: freeBooking.serviceAssignmentDto,
      startDateTimeDto: {
        localDateDto: this.searchRequest.localDateDto,
        localTimeDto: startTime
      },
      client: null,
      bookingState: null
    }
    this.bookingApiService.bookService(bookingDto).subscribe(
      res =>{
        // porównac bookingi ? 
        this.snackBar.open("Booked");
        this.getFreeBookings();
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    );
  }
}

