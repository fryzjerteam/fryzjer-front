import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeDatesComponent } from './free-dates.component';

describe('FreeDatesComponent', () => {
  let component: FreeDatesComponent;
  let fixture: ComponentFixture<FreeDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
