import { ExceptionsService } from '@exceptions/exceptions.service';
import { ServicesApiService } from '@core/api-services/services-api.service';
import { GenBarber } from '@core/dtos/barber-dto';
import { Component, OnInit } from '@angular/core';
import { BookingApiService } from '@core/api-services/booking-api.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  services: GenBarber.ServiceDto[];
  datePicker: Date;
  selectedService: GenBarber.ServiceDto;
  searchFreeBookings: GenBarber.SearchFreeBookingDts;

  minDate: Date = new Date(Date.now());
  maxDate: Date = new Date();

  constructor(private servicesService: ServicesApiService,
              private exceptionsService: ExceptionsService,
              private bookingApiService: BookingApiService) { }

  ngOnInit() {
    this.maxDate.setDate(this.minDate.getDate() + 30);
    
    this.servicesService.getAllServices().subscribe(
      res => {
        this.services = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    );
  }

  onServiceSelect(service: GenBarber.ServiceDto){
    if(this.selectedService != service){
      this.datePicker = null;
      this.searchFreeBookings = null;
      this.selectedService = service;
    }
  }

  onDateChange(event){
    if(this.selectedService != null && this.datePicker != null){

      let localDateDto: GenBarber.LocalDateDto = {
        month: this.datePicker.getMonth() + 1,
        year: this.datePicker.getFullYear(),
        day: this.datePicker.getDate()
      }

      this.searchFreeBookings = {
        localDateDto: localDateDto,
        serviceDto: this.selectedService
      }      
    }
  }

}
