import { MyIncomingBookingsComponent } from './my-incoming-bookings/my-incoming-bookings.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { HomeComponent } from './home.component';
import { Role } from '@core/security/roles';
import { AuthGuard } from '@core/security/auth-guard.service';
import { BookingComponent } from '@app/home/booking/booking.component';
import { MyPastBookingsComponent } from '@app/home/my-past-bookings/my-past-bookings.component';
import { BarberContactComponent } from '@app/home/barber-contact/barber-contact.component';
import { MyAccountComponent } from '@app/home/my-account/my-account.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    data: { role: Role.CLIENT },
    children: [
      {
        path: '',
        redirectTo: '/home/initialPage',
        pathMatch: 'full',
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'initialPage',
        component: InitialPageComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'booking',
        component: BookingComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'my-incoming-bookings',
        component: MyIncomingBookingsComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'my-past-bookings',
        component: MyPastBookingsComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'barber-contact',
        component: BarberContactComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
      {
        path: 'myaccount',
        component: MyAccountComponent,
        canActivate: [AuthGuard],
        data: { role: Role.CLIENT },
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
