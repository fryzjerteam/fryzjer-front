import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarberContactComponent } from './barber-contact.component';

describe('BarberContactComponent', () => {
  let component: BarberContactComponent;
  let fixture: ComponentFixture<BarberContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarberContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarberContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
