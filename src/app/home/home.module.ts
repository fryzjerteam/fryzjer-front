import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ServicesApiService } from '@core/api-services/services-api.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { LayoutModule } from '../layout/layout.module';

import { HomeComponent } from './home.component';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { HomeRoutingModule } from "./home-routing.module";
import { BookingComponent } from './booking/booking.component';
import { FreeDatesComponent } from './booking/free-dates/free-dates.component';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { MyIncomingBookingsComponent } from './my-incoming-bookings/my-incoming-bookings.component';
import { MyPastBookingsComponent } from './my-past-bookings/my-past-bookings.component';
import { BarberContactComponent } from './barber-contact/barber-contact.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { PassChangeDlgComponent } from './my-account/pass-change-dlg/pass-change-dlg.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    LayoutModule,
    HomeRoutingModule
  ],
  providers: [
    ServicesApiService, 
    BookingApiService,
    UsersApiService
  ],
  declarations: [
    HomeComponent,
    InitialPageComponent,
    BookingComponent,
    FreeDatesComponent,
    MyIncomingBookingsComponent,
    MyPastBookingsComponent,
    BarberContactComponent,
    MyAccountComponent,
    PassChangeDlgComponent
  ],
  exports: [
    HomeComponent,
    InitialPageComponent,
  ],
  entryComponents:[
    PassChangeDlgComponent
  ]
})
export class HomeModule {}
