import { ExceptionsService } from '@exceptions/exceptions.service';
import { BookingApiService } from '@app/core/api-services/booking-api.service';
import { GenBarber } from '@core/dtos/barber-dto';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-incoming-bookings',
  templateUrl: './my-incoming-bookings.component.html',
  styleUrls: ['./my-incoming-bookings.component.scss']
})
export class MyIncomingBookingsComponent implements OnInit {

  private bookings: GenBarber.BookingDto[];

  constructor(
    private bookingApiService: BookingApiService,
    private exceptionsService: ExceptionsService
  ) { }

  ngOnInit() {
    this.getIncomingBookings();
  }

  private getIncomingBookings(){
    this.bookingApiService.getFutureClientBookings().subscribe(
      res => {
        this.bookings = res.body;
      },
      err => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  private onBookingClick(booking: GenBarber.BookingDto){
    // TODO Implement
  }

}
