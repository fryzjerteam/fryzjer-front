import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyIncomingBookingsComponent } from './my-incoming-bookings.component';

describe('MyIncomingBookingsComponent', () => {
  let component: MyIncomingBookingsComponent;
  let fixture: ComponentFixture<MyIncomingBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyIncomingBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyIncomingBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
