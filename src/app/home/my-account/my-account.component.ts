import { PassChangeDlgComponent } from './pass-change-dlg/pass-change-dlg.component';
import { YesNoDlgData } from '@app/core/interfaces/yes-no-dlg-data';
import { YesNoDlgComponent } from './../../core/dialogs/yes-no-dlg/yes-no-dlg.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { GenUsers } from '@app/core/dtos/users-dto';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {

  constructor(
    private usersApiService: UsersApiService,
    private exceptionsService: ExceptionsService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  private contactDataChange = false;
  private userDetails: GenUsers.UserDetailsDto = {    
      address: "",
      email: "",
      phone: "",
      userId: null    
  }

  ngOnInit() {
    this.usersApiService.getCurrentUserDetails().subscribe(
      (res: any ) => {
        this.userDetails = res.body;
      },
      (err: any ) => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  onChangeDataClick(){
    this.contactDataChange = true;
  }

  onChangeDataButtonClick(){
    let data: YesNoDlgData = {
      titel: "bookings.areYouSure",
      content: ""
    }
    const dialogRef = this.dialog.open(YesNoDlgComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == true){
        this.usersApiService.updateCurrentUserDetails(this.userDetails).subscribe(
          res => {
            if(res.body != null){
              this.snackBar.open("OK");
            }
            else{
              this.snackBar.open("ERROR");
            }
          },
          err => {
            this.exceptionsService.handleRestException(err);
          }
        )
      }
    });
    this.contactDataChange = false;
  }

  onPasswordChangeClick(){
    this.dialog.open(PassChangeDlgComponent);
  }

}
