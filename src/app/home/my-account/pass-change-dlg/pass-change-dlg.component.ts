import { GenUsers } from '@core/dtos/users-dto.ts';
import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { ExceptionsService } from '@app/exceptions/exceptions.service';
import { MatSnackBar, MatDialogClose, MatDialogRef } from '@angular/material';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-pass-change-dlg',
  templateUrl: './pass-change-dlg.component.html'
})
export class PassChangeDlgComponent {

  constructor(
    private usersApiService: UsersApiService,
    private exceptionService: ExceptionsService,
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PassChangeDlgComponent>
  ) { }

  private passForm = this.formBuilder.group({
    oldPass: [""],
    newPass1: [""],
    newPass2: [""],
  })

  private onChangePass(){
    let pass: GenUsers.PassChangeDto = {
      oldPass: this.passForm.get("oldPass").value,
      newPass1: this.passForm.get("newPass1").value,
      newPass2: this.passForm.get("newPass2").value
    }
    this.usersApiService.changeUsersPassword(pass).subscribe(
      res => {
        let response: GenUsers.PassChangeResponse = res.body;
        if(response.type == "OK"){
          this.snackBar.open("OK");
        }
        else{
          this.snackBar.open(response.type, response.message);
        }
        this.dialogRef.close();
      },
      err => {
        this.exceptionService.handleRestException(err);
      }
    )
  }

}
