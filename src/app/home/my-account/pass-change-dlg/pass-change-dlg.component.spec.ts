import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassChangeDlgComponent } from './pass-change-dlg.component';

describe('PassChangeDlgComponent', () => {
  let component: PassChangeDlgComponent;
  let fixture: ComponentFixture<PassChangeDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassChangeDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassChangeDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
