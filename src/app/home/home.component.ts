import { NaviService } from '@core/services/navi.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TdMediaService } from '@covalent/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { AppInterface } from '@app/core/interfaces/app-interface';

@Component({
  selector: 'public-home',
  templateUrl: '../layout/main-content.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ NaviService ]
})
export class HomeComponent implements OnInit{

  private naviItems: AppInterface.NaviItem[];
  private cardTitle: string;
  private data: any[] = ['sample'];
  private disableHeader: boolean = true;

  sideNavOpened: boolean = false;
  constructor(private router: Router, public media: TdMediaService, private naviService: NaviService) {}
  
  ngOnInit(): void {
    this.naviItems = this.naviService.getHomeNaviItems();
  }

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onToggle(value: boolean): void {
    this.sideNavOpened = value;
  }

  close(titel: string): void {
    this.sideNavOpened = false;
    if(titel == "home"){
      this.cardTitle = "";
      this.disableHeader = true;
    }
    else{
      this.cardTitle = titel;
      this.disableHeader = false;
    }
  }

  onActivate() {
  } 
}
