import { ApplicationSettingsService } from './application-settings.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AuthService } from '../security/auth.service';
import { ExceptionsService } from "@exceptions/exceptions.service";
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';

@NgModule({})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        AuthService, 
        ExceptionsService, 
        ApplicationSettingsService,
        {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
        {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {width: '250px'}}
      ]
    };
  }
} 