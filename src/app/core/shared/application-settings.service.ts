import { RequestOptions } from '@core/request-options';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BusinessOperationsService } from '@core/shared/business-operations.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ApplicationSettingsService {

  constructor(private BO: BusinessOperationsService,
              private http: HttpClient) { }

  getUnitOffsetTime(): Observable<any>{
    return this.http.get(this.BO.applicationSettings() + "/unitOffset", new RequestOptions.JsonStandard());
  }


}
