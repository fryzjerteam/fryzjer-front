import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';


export class BusinessOperationsService {
  public serverPath: string = environment.restServiceRoot;
  public restPath: string = environment.restPathRoot;

  login(): string {
    return this.serverPath + 'login';
  }
  logout(): string {
    return this.serverPath + 'logout';
  }
  getCsrf(): string {
    return this.serverPath + 'security/v1/csrftoken';
  }

  // ----------------------USERS-------------------------------
  adminUser(): string {
    return this.serverPath + 'users/user';
  }
  registerClient(): string {
    return this.serverPath + 'users/registerClient';
  }
  searchUsers(): string {
    return this.serverPath + 'users/findUsersByName';
  }
  searchClients(): string {
    return this.serverPath + 'users/findClientsByName';
  }
  findAllUsersByRole(): string {
    return this.serverPath + 'users/findUsersByRole';
  }
  private userDetails(): string {
    return this.serverPath + 'users/userDetails';
  }
  createUserDetails(): string {
    return this.userDetails();
  }
  getUserDetails(): string {
    return this.userDetails();
  }
  currentUserDetails(): string {
    return this.serverPath + 'users/currentUserDetails';
  }
  changeUserPassword(): string {
    return this.serverPath + 'users/changePass';
  } 

  // --------------------- BARBER ------------------------------
  getAllServices(): string {
    return this.serverPath + 'barber/services/all';
  }
  services(): string {
    return this.serverPath + 'barber/services/service';
  }
  assignService(): string {
    return this.serverPath + 'barber/serviceAssignment/assignment';
  }
  deleteServiceAssignment(): string {
    return this.serverPath + 'barber/serviceAssignment/deleteAssignment';
  }
  findServiceAssignmentByUser(): string {
    return this.serverPath + 'barber/serviceAssignment/findByUser';
  }  
  findScheduleByUser(): string {
    return this.serverPath + 'barber/schedules/findByUser';
  }
  schedule(): string {
    return this.serverPath + 'barber/schedules/schedule';
  }

  //---------------------- BOOKING -------------------------------
  private booking(): string {
    return this.serverPath + 'bookings/booking';
  }
  findFreeBookings(): string {
    return this.serverPath + 'bookings/freeBookings';
  }
  bookService(): string {
    return this.booking();
  }
  getBookingsByEmployee(): string {
    return this.serverPath + 'bookings/bookingsByEmployee';
  }
  getBookingsByCurrentEmployee(): string {
    return this.serverPath + 'bookings/bookingsByCurrentEmployee';
  }
  getBookingsByDay(): string {
    return this.serverPath + 'bookings/bookingsByDay';
  }
  getBookingsCanceledByDay(): string {
    return this.serverPath + 'bookings/bookingsCanceledByDay';
  }
  getBookingsByService(): string {
    return this.serverPath + 'bookings/bookingsByService';
  }
  getFutureClientBookings(): string {
    return this.serverPath + 'bookings/futureClientBookings';
  }
  getOldClientBookings(): string {
    return this.serverPath + 'bookings/oldClientBookings';
  }
  cancelBooking(): string {
    return this.serverPath + 'bookings/cancelBooking';
  }

  // --------------------- GENERAL ----------------------------
  applicationSettings(): string {
    return this.serverPath + 'applicationSettings';
  }

}
