import { TestBed, inject } from '@angular/core/testing';

import { NaviService } from './navi.service';

describe('NaviService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NaviService]
    });
  });

  it('should be created', inject([NaviService], (service: NaviService) => {
    expect(service).toBeTruthy();
  }));
});
