import { Injectable } from '@angular/core';
import { AppInterface } from "../interfaces/app-interface";
  

@Injectable()
export class NaviService {
  
  constructor() { }
  
  private title: string;
  private barberNaviItems: AppInterface.NaviItem[] = 
  [
    {
      titel: 'barber.mycalendar',
      url: "mycalendar",
      avatar: 'date_range',
      description: 'barber.mycalendarDescription'
    },
    {
      titel: 'barber.bookingDay',
      url: "booking-day",
      avatar: 'event',
      description: 'barber.bookingDayDescription'
    },
    {
      titel: 'barber.clients',
      url: "clients",
      avatar: 'people',
      description: 'barber.clientsDescription'
    },
    {
      titel: 'barber.services',
      url: "services",
      avatar: 'group_work',
      description: 'barber.servicesMan'
    },
    {
      titel: 'barber.serviceAssignment',
      url: "service-assignment",
      avatar: 'assignment_ind',
      description: 'barber.servicesMan'
    },
  ]

  private adminNaviItems: AppInterface.NaviItem[] = 
  [
    {
      titel: 'admin.users',
      url: "users",
      avatar: 'perm_identity',
      description: 'admin.usersMan'
    }
  ]

  private homeNaviItems: AppInterface.NaviItem[] = 
  [
    {
      titel: 'client.booking',
      url: "booking",
      avatar: 'event',
      description: 'client.bookingDescription'
    },
    {
      titel: 'client.myIncomingBookings',
      url: "my-incoming-bookings",
      avatar: 'alarm',
      description: 'client.myIncomingBookingsDescription'
    },
    {
      titel: 'client.myPastBookings',
      url: "my-past-bookings",
      avatar: 'history',
      description: 'client.myPastBookingsDescription'
    },
    {
      titel: 'client.barberContact',
      url: "barber-contact",
      avatar: 'phone',
      description: 'client.barberContactDescription'
    },
    {
      titel: 'client.myaccount',
      url: "myaccount",
      avatar: 'settings_applications',
      description: 'client.myaccountDescription'
    }
  ]

  public getBarberNaviItems() : AppInterface.NaviItem[]{
    return this.barberNaviItems;
  }

  public getAdminNaviItems(): AppInterface.NaviItem[] {
    return this.adminNaviItems;
  }

  public getHomeNaviItems(): AppInterface.NaviItem[] {
    return this.homeNaviItems;
  }

  public setTitle(title: string){
    this.title=title;
  }
  public getTitle(): string {
    return this.title;
  }
}
