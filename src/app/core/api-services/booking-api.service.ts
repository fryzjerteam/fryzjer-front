import { RequestOptions } from '@core/request-options';
import { Observable } from 'rxjs';
import { BusinessOperationsService } from '@core/shared/business-operations.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenBarber } from '@app/core/dtos/barber-dto';

@Injectable()
export class BookingApiService {

  constructor(  private BO: BusinessOperationsService,
                private http: HttpClient) { }

  getFreeBookings(searchDto: GenBarber.SearchFreeBookingDts): Observable<any>{
    return this.http.post(this.BO.findFreeBookings(), searchDto, new RequestOptions.JsonStandard());
  }

  bookService(bookingDto: GenBarber.BookingDto): Observable<any>{
    return this.http.post(this.BO.bookService(), bookingDto, new RequestOptions.JsonStandard());
  }

  getBookingsByEmployee(request: GenBarber.SearchBookingsDts): Observable<any>{
    return this.http.post(this.BO.getBookingsByEmployee(), request, new RequestOptions.JsonStandard());
  }

  getBookingsByCurrentEmployee(request: GenBarber.SearchBookingsDts): Observable<any>{
    return this.http.post(this.BO.getBookingsByCurrentEmployee(), request, new RequestOptions.JsonStandard());
  }

  getBookingsByDay(request: GenBarber.SearchBookingsDts): Observable<any>{
    return this.http.post(this.BO.getBookingsByDay(), request, new RequestOptions.JsonStandard());
  }

  getBookingsCanceledByDay(request: GenBarber.SearchBookingsDts): Observable<any>{
    return this.http.post(this.BO.getBookingsCanceledByDay(), request, new RequestOptions.JsonStandard());
  }

  getBookingsByService(request: GenBarber.SearchBookingsDts): Observable<any>{
    return this.http.post(this.BO.getBookingsByService(), request, new RequestOptions.JsonStandard());
  }

  getFutureClientBookings(): Observable<any>{
    return this.http.get(this.BO.getFutureClientBookings(), new RequestOptions.JsonStandard());
  }

  getOldClientBookings(): Observable<any>{
    return this.http.get(this.BO.getOldClientBookings(), new RequestOptions.JsonStandard());
  }

  cancelBooking(cancelBookingDto: GenBarber.CancelBookingDto): Observable<any>{
    return this.http.post(this.BO.cancelBooking(), cancelBookingDto, new RequestOptions.JsonStandard());
  }

}
