import { GenBarber } from '@core/dtos/barber-dto';
import { GenUsers } from '@core/dtos/users-dto';
import { Injectable } from '@angular/core';
import { RequestOptions } from '@core/request-options';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BusinessOperationsService } from '@core/shared/business-operations.service';

@Injectable()
export class ScheduleApiService {

  constructor(private BO: BusinessOperationsService,
    private http: HttpClient) { }

  findScheduleByEmployee(employee: GenUsers.UserDto): Observable<any>{
    return this.http.post(this.BO.findScheduleByUser(), employee, new RequestOptions.JsonStandard());
  }

  updateSchedule(schedule: GenBarber.ScheduleDto): Observable<any>{
    return this.http.put(this.BO.schedule(), schedule, new RequestOptions.JsonStandard());
  }
}
