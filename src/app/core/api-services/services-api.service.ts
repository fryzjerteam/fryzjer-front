import { RequestOptions } from '@core/request-options';
import { GenUsers } from '@core/dtos/users-dto';
import { GenBarber } from '@core/dtos/barber-dto';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BusinessOperationsService } from '@core/shared/business-operations.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ServicesApiService {

  constructor(private BO: BusinessOperationsService,
    private http: HttpClient) { }

  getAllServices(): Observable<any>{
    return this.http.get(this.BO.getAllServices(), new RequestOptions.JsonStandard());
  }

  addService(serviceDto: GenBarber.ServiceDto): Observable<any>{
    return this.http.post(this.BO.services(), serviceDto, new RequestOptions.JsonStandard());
  }

  updateService(serviceDto: GenBarber.ServiceDto): Observable<any>{
    return this.http.put(this.BO.services(), serviceDto, new RequestOptions.JsonStandard());
  }

  deleteService(serviceDto: GenBarber.ServiceDto): Observable<any>{
    let params = new HttpParams().set("id",serviceDto.id.toString());
    return this.http.delete(this.BO.services(), new RequestOptions.JsonParams(params));
  }

  assignService(serviceAssignment: GenBarber.ServiceAssignmentDto): Observable<any>{
    return this.http.post(this.BO.assignService(), serviceAssignment, new RequestOptions.JsonStandard());
  }

  deleteServiceAssignment(serviceAssignment: GenBarber.ServiceAssignmentDto): Observable<any>{
    return this.http.post(this.BO.deleteServiceAssignment(), serviceAssignment, new RequestOptions.JsonStandard());
  }

  findServiceAssignmentsByUser(userDto: GenUsers.UserDto): Observable<any>{
    return this.http.post(this.BO.findServiceAssignmentByUser(), userDto, new RequestOptions.JsonStandard());
  }


}
