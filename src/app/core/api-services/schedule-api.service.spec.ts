import { TestBed, inject } from '@angular/core/testing';

import { ScheduleApiService } from './schedule-api.service';

describe('ScheduleApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScheduleApiService]
    });
  });

  it('should be created', inject([ScheduleApiService], (service: ScheduleApiService) => {
    expect(service).toBeTruthy();
  }));
});
