
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BusinessOperationsService } from '@core/shared/business-operations.service';
import { Role } from '@core/security/roles';
import { GenUsers } from "@core/dtos/users-dto";
import { RequestOptions } from "@core/request-options";
@Injectable()
export class UsersApiService {
  constructor(private BO: BusinessOperationsService, private http: HttpClient) { }
  searchUsers(name: string): Observable<any> {
    let params = new HttpParams().set("name", name);
    return this.http.get(this.BO.searchUsers(), new RequestOptions.JsonParams(params));
  }
  searchClients(name: string): Observable<any> {
    let params = new HttpParams().set("name", name);
    return this.http.get(this.BO.searchClients(), new RequestOptions.JsonParams(params));
  }
  registerUser(userDataDts: GenUsers.UserRegisterDataDts): Observable<any> {
    return this.http.post(this.BO.adminUser(), userDataDts, new RequestOptions.JsonStandard());
  }
  registerClient(userDataDts: GenUsers.UserRegisterDataDts): Observable<any> {
    return this.http.post(this.BO.registerClient(), userDataDts, new RequestOptions.JsonStandard());
  }
  getUser(id: number): Observable<any> {
    let params = new HttpParams().set("id", id.toString());
    return this.http.get(this.BO.adminUser(), new RequestOptions.JsonParams(params));
  }
  updateUser(userDataDts: GenUsers.UserDataDts): Observable<any> {
    return this.http.put(this.BO.adminUser(), userDataDts, new RequestOptions.JsonStandard());
  }
  deleteUser(id: number): Observable<any> {
    let params = new HttpParams().set("id", id.toString());
    return this.http.delete(this.BO.adminUser(), new RequestOptions.JsonParams(params));
  }
  getUsersByRole(role: Role): Observable<any> {
    let params = new HttpParams().set("role", <any>role.valueOf());
    return this.http.get(this.BO.findAllUsersByRole(), new RequestOptions.JsonParams(params));
  }
  createUserDetails(userDetailsDto: GenUsers.UserDetailsDto): Observable<any> {
    return this.http.post(this.BO.createUserDetails(), userDetailsDto, new RequestOptions.JsonStandard());
  }
  getUserDetails(userId: number): Observable<any> {
    let params = new HttpParams().set("userId", userId.toString());
    return this.http.get(this.BO.getUserDetails(), new RequestOptions.JsonParams(params));
  }
  getCurrentUserDetails(): Observable<any> {
    return this.http.get(this.BO.currentUserDetails(), new RequestOptions.JsonStandard());
  }
  updateCurrentUserDetails(userDetails: GenUsers.UserDetailsDto): Observable<any> {
    return this.http.put(this.BO.currentUserDetails(), userDetails, new RequestOptions.JsonStandard());
  }
  changeUsersPassword(passwordDto : GenUsers.PassChangeDto): Observable<any> {
    return this.http.post(this.BO.changeUserPassword(), passwordDto, new RequestOptions.JsonStandard());
  }
}