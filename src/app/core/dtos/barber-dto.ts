import { GenUsers } from '@core/dtos/users-dto.ts';
export declare namespace GenBarber {
    export interface ServiceDto {
        id: number;
        name: string;
        durationUnits: number;
      }
    
    export interface ServiceAssignmentDto {
        serviceDto: ServiceDto;
        userDto: GenUsers.UserDto;
    }

    
    export interface ScheduleDto {
        id: number;
        userDto: GenUsers.UserDto;
        days: ScheduleDayDto[];        
    }
    
    export interface ScheduleDayDto {
        startTime: LocalTimeDto;
        endTime: LocalTimeDto;
        dayName: string;
    }
    
    export interface LocalTimeDto {
        hour: number;
        minute: number;
        second: number;
    }

    export interface LocalDateDto {
        month: number;
        year: number;
        day: number;
    }

    export interface LocalDateTimeDto {
        localDateDto: LocalDateDto;
        localTimeDto: LocalTimeDto;
    }

    export interface SearchFreeBookingDts {
        localDateDto: LocalDateDto;
        serviceDto: ServiceDto;
    }

    export interface FreeBookingDto {   
        startTimes: LocalTimeDto[];
        serviceAssignmentDto: ServiceAssignmentDto;
    }

    export enum BookingState {
        ACTIVE, CANCELLED
    }    

    export interface BookingDto {
        id: number;
        serviceAssignmentDto: ServiceAssignmentDto;
        startDateTimeDto: LocalDateTimeDto;
        client: GenUsers.UserDto;
        bookingState: BookingState;
    }

    export interface SearchBookingsDts {
        userDto: GenUsers.UserDto;
        serviceDto: ServiceDto;
        startDate: LocalDateDto;
        endDate: LocalDateDto;
    }

    export interface CancelBookingDto {
        bookingId: number;
        note: string;
    }

      
}