import { PageData } from './../interfaces/page-data';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'minute'})
export class MinutePipe implements PipeTransform {
  transform(value: number): string {
    if(value < 10){
        return "0" + value.toString();
    }
    return value.toString();
  }
}