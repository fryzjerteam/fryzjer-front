import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { YesNoDlgData } from '@app/core/interfaces/yes-no-dlg-data';

@Component({
  selector: 'app-yes-no-dlg',
  templateUrl: './yes-no-dlg.component.html'
})
export class YesNoDlgComponent {

  constructor(
    public dialogRef: MatDialogRef<YesNoDlgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: YesNoDlgData
  ) { }
}
