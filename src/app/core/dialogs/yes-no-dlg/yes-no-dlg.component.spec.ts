import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YesNoDlgComponent } from './yes-no-dlg.component';

describe('YesNoDlgComponent', () => {
  let component: YesNoDlgComponent;
  let fixture: ComponentFixture<YesNoDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YesNoDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YesNoDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
