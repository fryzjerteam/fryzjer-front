
export declare namespace AppInterface {
    export interface NaviItem {
        titel: string;
        url: string;
        avatar: string;
        description: string;
      }
}