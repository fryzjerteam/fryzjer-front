
import { HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";

export namespace RequestOptions {
    
    export class JsonStandard {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        observe: "response";
        params?: HttpParams | {
            [param: string]: string | string[];
        };
        reportProgress?: boolean;
        responseType?: "json";
        withCredentials?: boolean;
        constructor() {
            this.withCredentials = true;
            this.observe = 'response';
            this.responseType = 'json';
        }
    }
    export class JsonParams extends JsonStandard {
        params: HttpParams;
        constructor(params: HttpParams){
            super();
            this.params = params;
        }
    }
}