import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { Role } from '@core/security/roles';
import { AuthGuard } from '@core/security/auth-guard.service';

const routes: Routes = [
    {       
        path: '', 
        component: AdminComponent,
        canActivate: [AuthGuard],
        data:{role:Role.ADMIN},
        children: [
          {
            path: '',
            redirectTo: '/admin/initialPage',
            pathMatch: 'full',
            canActivate: [AuthGuard],
            data:{role:Role.ADMIN}, 
          },
          {
            path: 'initialPage',
            component: InitialPageComponent,
            canActivate: [AuthGuard],
            data:{role:Role.ADMIN}, 
          },
          {
            path: 'users',
            component: UsersComponent,
            canActivate: [AuthGuard],
            data:{role:Role.ADMIN}, 
          },
          {
            path: 'users/add-user',
            component: AddUserComponent,
            canActivate: [AuthGuard],
            data:{role:Role.ADMIN}, 
          }
        ], 
      },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
