import { NaviService } from '@core/services/navi.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TdMediaService } from '@covalent/core';
import { AppInterface } from '@app/core/interfaces/app-interface';

@Component({
  selector: 'app-admin',
  templateUrl: '../layout/main-content.component.html',
  providers: [NaviService]
})
export class AdminComponent implements OnInit {

  private naviItems: AppInterface.NaviItem[];
  private cardTitle: string;

  sideNavOpened: boolean = false;
  constructor(
    private router: Router,
    public media: TdMediaService,
    private naviService: NaviService
  ) { }

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onToggle(value: boolean): void {
    this.sideNavOpened = value;
  }

  close(title: string): void {
    this.sideNavOpened = false;
    if (title == "home") {
      title = "admin.home";
    }
    this.cardTitle = title;
  }

  ngOnInit() {
    this.naviItems = this.naviService.getAdminNaviItems();
    this.cardTitle = "admin.home";
  }

  onActivate() {
    if (this.naviService.getTitle() != null) {
      this.cardTitle = this.naviService.getTitle();
      this.naviService.setTitle(null);
    }
  }

}
