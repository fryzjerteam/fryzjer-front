import { UsersApiService } from "@app/core/api-services/users-api.service";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { InitialPageComponent } from './initial-page/initial-page.component';

import { CoreModule } from '../core/core.module';
import { LayoutModule } from '../layout/layout.module';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AddUserComponent } from './users/add-user/add-user.component';
import { UserDetailDlgComponent } from './users/user-detail-dlg/user-detail-dlg.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    LayoutModule,
    AdminRoutingModule  
  ],
  declarations: [AdminComponent,
  InitialPageComponent,
  UsersComponent,
  AddUserComponent,
  UserDetailDlgComponent],
  exports: [AdminComponent],
  providers:[UsersApiService],
  entryComponents:[
    UserDetailDlgComponent
  ]
})
export class AdminModule { }
