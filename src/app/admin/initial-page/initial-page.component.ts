import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NaviService } from '@app/core/services/navi.service';
import { AppInterface } from '@app/core/interfaces/app-interface';

@Component({
  selector: 'app-initial-page',
  templateUrl: '../../layout/standard-initial-page.html'
})
export class InitialPageComponent implements OnInit {

  constructor(
    private router: Router,
    private naviService: NaviService
  ) { }
  private items: AppInterface.NaviItem[];

  ngOnInit() {
    this.items = this.naviService.getAdminNaviItems();
  }

  onSelect(item: AppInterface.NaviItem) {
    this.naviService.setTitle(item.titel);
    this.router.navigate(['admin/' + item.url]);
  }

}
