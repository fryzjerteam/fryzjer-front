import { Component, OnInit } from '@angular/core';
import { Role } from '@core/security/roles';
import { GenUsers } from "@core/dtos/users-dto";
import { ExceptionsService } from '@exceptions/exceptions.service';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder } from '@angular/forms';


export interface RoleView {
  value: Role;
  viewValue: string;
}

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  providers: [UsersApiService]
})
export class AddUserComponent {

  constructor(
    private usersApiService: UsersApiService,
    private exceptionService: ExceptionsService,
    public snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) { }

  private addUserForm = this.formBuilder.group({
    login: [""],
    password: [""],
    firstName: [""],
    lastName: [""],
    address: [""],
    email: [""],
    phone: [""],
    selectedRole: []
  })

  private roles: RoleView[] = [
    { value: Role.ADMIN, viewValue: 'Admin' },
    { value: Role.CLIENT, viewValue: 'Client' },
    { value: Role.EMPLOYEE, viewValue: 'Employe' }
  ];

  onAddButton() {
    let userDataDts: GenUsers.UserRegisterDataDts = {

      basicUserDto: {
        id: null,
        name: this.addUserForm.get("login").value,
        password: this.addUserForm.get("password").value,
        firstName: this.addUserForm.get("firstName").value,
        lastName: this.addUserForm.get("lastName").value,
        role: Role[this.addUserForm.get("selectedRole").value]
      },
      userDetailsDto: {
        userId: null,
        address: this.addUserForm.get("address").value,
        email: this.addUserForm.get("email").value,
        phone: this.addUserForm.get("phone").value,
      }
    };

    this.usersApiService.registerUser(userDataDts).subscribe(
      (res: any) => {
        let receivedUserData: GenUsers.UserDataDts = res.body;
        if (receivedUserData == null) {
          this.snackBar.open("User with this login already exists");
        }
        else {
          this.snackBar.open('OK');
        }
      },
      (err: any) => {
        this.exceptionService.handleRestException(err);
      },
    ) 
  }
}
