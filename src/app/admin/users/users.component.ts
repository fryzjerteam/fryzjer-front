import { MatSnackBar, MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { UsersApiService } from "@app/core/api-services/users-api.service";
import { ExceptionsService } from '@exceptions/exceptions.service';
import { GenUsers } from '@app/core/dtos/users-dto';
import { UserDetailDlgComponent } from '@app/admin/users/user-detail-dlg/user-detail-dlg.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {

  constructor(
    private UsersApiService: UsersApiService,
    private exceptionService: ExceptionsService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  private users: GenUsers.UserDto[];
  private isUserListVisible = false;
  private searchString: string;


  onSearch(name: string) {
    this.searchString = name;
    this.UsersApiService.searchUsers(name).subscribe(
      (res: any) => {
        this.users = res.body;

        if (this.users.length == 0) {
          this.snackBar.open("Cannot find the user");
          this.isUserListVisible = false;
        }
        else {
          this.isUserListVisible = true;
        }
      },
      (err: any) => {
        this.exceptionService.handleRestException(err);
      }
    )
  }

  onSelect(user: GenUsers.UserDto) {
    if (user != null && user.id != null) {
      const dialogRef = this.dialog.open(UserDetailDlgComponent,
        {
          width: "40%",
          data: user.id
        }
      );
      dialogRef.afterClosed().subscribe(result => {
        if(result === true){
          this.onSearch(this.searchString);
        }
      });
    }
  }

}

