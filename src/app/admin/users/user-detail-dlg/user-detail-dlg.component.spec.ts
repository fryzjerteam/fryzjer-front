import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailDlgComponent } from './user-detail-dlg.component';

describe('UserDetailDlgComponent', () => {
  let component: UserDetailDlgComponent;
  let fixture: ComponentFixture<UserDetailDlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailDlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailDlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
