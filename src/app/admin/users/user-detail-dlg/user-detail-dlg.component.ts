import { ExceptionsService } from '@exceptions/exceptions.service';
import { UsersApiService } from '@app/core/api-services/users-api.service';
import { Component, OnInit, Inject } from '@angular/core';
import { UserDetailsDlgComponent } from '@app/barber/booking-day/dialogs/user-details-dlg/user-details-dlg.component';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { GenUsers } from '@app/core/dtos/users-dto';

@Component({
  selector: 'app-user-detail-dlg',
  templateUrl: './user-detail-dlg.component.html'
})
export class UserDetailDlgComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UserDetailsDlgComponent>,
    @Inject(MAT_DIALOG_DATA) public userId: number,
    private usersApiService: UsersApiService,
    private exceptionsService: ExceptionsService,
    private snachBar: MatSnackBar
  ) { }

  private userData: GenUsers.UserDataDts = {
    userDto: {
      id: null,
      firstName: "",
      lastName: "",
      name: "",
      role: ""
    },
    userDetailsDto: {
      address: "",
      email: "",
      phone: "",
      userId: null
    }
  }

  ngOnInit() {
    this.usersApiService.getUser(this.userId).subscribe(
      (res: any ) => {
        this.userData = res.body;
      },
      (err: any ) => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  onEdit(){
    this.usersApiService.updateUser(this.userData).subscribe(
      (res: any) =>{
        this.snachBar.open("User updated");  
      },
      (err: any) => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

  onRemove(){
    //todo popup
    this.usersApiService.deleteUser(this.userData.userDto.id).subscribe(
      (res: any) =>{
        this.snachBar.open("User removed");  
        this.dialogRef.close(true); 
      },
      (err: any) => {
        this.exceptionsService.handleRestException(err);
      }
    )
  }

}
